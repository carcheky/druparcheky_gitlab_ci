# Instructions

- copy .gitlab-ci.yml from this repo to your repo
- run
      ```bash
      composer require drush/drush
      ```
- run
      ```bash
      composer require bex/behat-screenshot dmore/behat-chrome-extension drupal/core-dev:^8 drupal/drupal-extensionemuse/behat-html-formatter --dev
      ```
- export your config to /config/sync


test
