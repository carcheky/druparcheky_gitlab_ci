@javascript @api
Feature: Register new user
 In order to register a new user
 I want to access /user/register/mezcalero
 So that I can register a new user

Scenario: 1 Register new user
Given I am an anonymous user
 When I go to "/user/register/mezcalero"
 And I enter "behat@test.test" for "edit-mail"
 And I enter "behat" for "edit-name"
 When I press "edit-submit"
 Then I go to "/"
 And I enter "behat" for "edit-name"
 When I press "edit-submit"
 Then I should see "You are now logged in as behat."


# Scenario: 4 Remove test user with drupal
# Given I run drupal "user:delete --user='behat'"

Scenario: 3 Remove test user with drush
Given I run drush "ucan behat -y --delete-content"

Scenario: Visit the homepage
Given I visit "/"
Then I should see "Bienvenido a Ciudad Mezcal"
